package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller<T extends Number> {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Number> values){
         return new IntegersBag<Number>(values);		
	}
	
	
	public static double getMean(IntegersBag<Number> bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag<Number> bag){
		return model.getMax(bag);
	}
	
	public static double getmin(IntegersBag<Number> bag){
		return model.getMin(bag);
	}
	
	public static double suma(IntegersBag<Number> bag){
		return model.suma(bag);
	}
	
	public static double cantidadDigitado(IntegersBag<Number> bag){
		return model.cantidaDigitados(bag);
	}
}
