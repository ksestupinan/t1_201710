package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations<T extends Number> {

	
	
	public double computeMean(IntegersBag<Number> bag){
		double mean = 0.00;
		double length = 0.00;
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next().doubleValue();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public double getMax(IntegersBag<Number> bag){
		double max = Integer.MIN_VALUE;
	    double value = 0.0;
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next().doubleValue();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public double getMin(IntegersBag<Number> bag)
	{
		double min = Integer.MAX_VALUE;
		double value;
		if(bag != null)
		{
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next().doubleValue();
				if(min > value)
				{
					min = value;
					
				}
			}
		}
		return min;
	}
	
	public double suma(IntegersBag<Number> bag)
	{
		double mean = 0;
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next().doubleValue();
			}
		}
		return mean;
	}
	
	public double cantidaDigitados(IntegersBag<Number> bag)
	{
		int c = 0;
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				c++;
				iter.next();
			}
		}
		return c;
	}
}
